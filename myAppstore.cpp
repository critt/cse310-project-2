#include <stdio.h>
#include <iostream>
#include <fstream>
#include <stdlib.h> //atoi

#define	CAT_NAME_LEN	25
#define	APP_NAME_LEN	50
#define	VERSION_LEN		10
#define	UNIT_SIZE		3

using namespace std;

struct categories
{
	char category[ CAT_NAME_LEN ]; // Name of category
	struct tree *root; // Pointer to root of search tree for this category
};

struct tree// A binary search tree
{ 
	struct app_info; // Information about the application
	struct tree *left; // Pointer to the left subtree
	struct tree *right; // Pointer to the right subtree
};

struct app_info
{
	char category[ CAT_NAME_LEN ]; // Name of category
	char app_name[ APP_NAME_LEN ]; // Name of the application
	char version[ VERSION_LEN ]; // Version number
	float size; // Size of the application
	char units[ UNIT_SIZE ]; // GB or MB
	float price; // Price in $ of the application
};

struct hash_table_entry
{
   char app_name[ APP_NAME_LEN ]; // Name of the application
   struct tree *app_node; // Pointer to node in tree containing the application information
};



int main()
{
	int numCategories = 0;
	int numApps = 0;
	int lineCounter = 1;


	
	for(string line; getline(cin, line);)//get numCategories from first line, incremented lineCounter and break
	{
		numCategories = atoi(line.c_str());
		lineCounter++;
		break;
	}

	categories *arrCategories = new categories[numCategories]; //allocate an array of categories of size numCategories

	for(string line; getline(cin, line);)
	{
		if (lineCounter > 1 && lineCounter < numCategories + 2)//read through list of categories stopping at line number containing the number of apps
		{

			//populate array of category structs
			//array[lineCounter - 2] <---this gives accurate index of array the line should go in
			for (int i = 0; i < line.length(); i++)//copy the category name into the categories struct in the given index of arrCategories[]
			{
				arrCategories[lineCounter - 2].category[i] = line.c_str()[i];
			}
			lineCounter++;
		}
		else if (lineCounter == numCategories + 2)//stop at line with number of apps and store that number
		{
			numApps = atoi(line.c_str());
			lineCounter++;
		}
		else if (lineCounter > numCategories + 2 && lineCounter < (numApps * 6 ) + (numCategories + 3))//proceed through bulk of lines processing app_info struct data correctly
		{
			//populate app_info structs somehow
		}
		else if (lineCounter == (numApps * 6 ) + (numCategories + 3))//stop at number of instructions (near end of file) and store that number
		{
			//store number of instructions (this line contains the number of instructions
		}
		else if (lineCounter > (numApps * 6 ) + (numCategories + 3))//process instructions (the rest of the file)
		{
			//process instructions (the rest of the file)
			if(line.substr(0,8) == "find app")
			{
				string appToFind = line.substr(9, line.length() - 9);
			}
			else if(line.substr(0,13) == "find category")
			{
				string catToFind = line.substr(14, line.length() - 14);
			}
			else if(line.substr(0,15) == "find price free")
			{
				//find free apps
			}
			else if(line.substr(0, 5) == "range")
			{
				//this one requires more parsing to determine correct outcome (i.e range Games app A F, range Games price 0.00 5.00)
				string delimeter = "\" ";
				string delimeter2 = " ";
				string rightChunk = line.substr(6, line.length() - 6);
				string catToRange = rightChunk.substr(0, rightChunk.find(delimeter) + 2);//+2 because it needs to follow through the delimeter, not end before it
				string kindOfRange;
				if(rightChunk.substr(rightChunk.find(delimeter) + 2, 3) == "app")
				{
					string compositeAppRange =  rightChunk.substr(rightChunk.find("app") + 4, rightChunk.length() - rightChunk.find("app") + 4);
					string rangeAppBegin = compositeAppRange.substr(0, compositeAppRange.find(delimeter2));
					string rangeAppEnd = compositeAppRange.substr(compositeAppRange.find(delimeter2) + 1, compositeAppRange.length() - compositeAppRange.find(delimeter2) + 1);
				}
				else if(rightChunk.substr(rightChunk.find(delimeter) + 2, 5) == "price")
				{
					string compositePriceRange =  rightChunk.substr(rightChunk.find("price") + 6, rightChunk.length() - rightChunk.find("price") + 6);
					string rangePriceBegin = compositeAppRange.substr(0, compositeAppRange.find(delimeter2));
					string rangePriceEnd = compositeAppRange.substr(compositeAppRange.find(delimeter2) + 1, compositeAppRange.length() - compositeAppRange.find(delimeter2) + 1);
				}

			}	
			else if(line.substr(0,6) == "delete")
			{
				//optional
			}
			
		}

	}



	//note: how to do input files in therminal: ./myAppstore < testInput.txt
	//test print (must pit in size manually)
	for (int i = 0; i < 24; i++)
	{
		cout << arrCategories[i].category << endl;
		cout << arrCategories[i].root << endl;
	}




}