#include <stdio.h>
#include <iostream>
#include <fstream>
#include <stdlib.h> //atoi, atof
#include <string.h>
#include <math.h>

#define	CAT_NAME_LEN	30
#define	APP_NAME_LEN	50
#define	VERSION_LEN		50
#define	UNIT_SIZE		5

using namespace std;

int globalNumCategories;
int globalNumApps;

struct app_info
{
	char category[ CAT_NAME_LEN ]; // Name of category
	char app_name[ APP_NAME_LEN ]; // Name of the application
	char version[ VERSION_LEN ]; // Version number
	float size; // Size of the application
	char units[ UNIT_SIZE ]; // GB or MB
	float price; // Price in $ of the application
};
/*struct tree// A binary search tree
{ 
	app_info aI; // Information about the application
	tree *left; // Pointer to the left subtree
	tree *right; // Pointer to the right subtree
	tree *parent;
};*/


struct tree{ // A 2-3 tree
    // In the case of a 2-node, only app_info and the left and right subtrees fields are used
    //    - app_info2 is empty and mid is NULL
    // In the case of a 3-node, all fields are used
 
	app_info aI; // Information about the application
    app_info aI2; // Information about the second application
	tree *left;  // Pointer to the left subtree; elements with keys < the key of app_info
	tree *mid; // Pointer to the middle subtree; elements with keys > key of app_info and < key app_info2
	tree *right;  // Pointer to the right subtree; elements with keys > key of app_info2
    tree *parent; // A pointer to the parent may be useful
};




struct categories
{
	char category[ CAT_NAME_LEN ]; // Name of category
	tree *root; // Pointer to root of search tree for this category
};
struct hash_table_entry
{
   char app_name[ APP_NAME_LEN ]; // Name of the application
   tree *app_node; // Pointer to node in tree containing the application information
};



int hashFunction(char app_name[APP_NAME_LEN], int size)
{
	int hash = 0;
	for (int i = 0; i < APP_NAME_LEN; i++)
	{
		hash = hash + app_name[i];
	}
	return hash % size;
}
bool TestForPrime(int val)
{
	int limit, factor = 2;
    limit = (long)( sqrtf( (float) val ) + 0.5f );
    while( (factor <= limit) && (val % factor) )
        factor++;
    return( factor > limit );
}
int getTableSize(int numApps)
{
	for (int i = 2*numApps; i++;)
	{
		if(TestForPrime(i))
		{
			return i;
		}
	}
}

app_info* makeApp_info(string category, string app_name, string version, string size, string units, string price)
{
	app_info *A = new app_info;
	for (int i = 0; i < category.length(); i++){ A->category[i] = category.c_str()[i]; }
	for (int i = 0; i < app_name.length(); i++){ A->app_name[i] = app_name.c_str()[i]; }
	for (int i = 0; i < version.length(); i++){ A->version[i] = version.c_str()[i]; }
	for (int i = 0; i < units.length(); i++){ A->units[i] = units.c_str()[i]; }
	A->size = atof(size.c_str());
	A->price = atof(price.c_str());
	/*cout << A->category << endl;
	cout << A->app_name << endl;
	cout << A->version << endl;
	cout << A->units << endl;
	cout << A->size << endl;
	cout << A->price << endl;*/
	return A;
}


//working :)
tree* insertTree(app_info* tmpAppInfo, categories ptrArrCategories[], int tmpNumCategories)
{
	tree* t = new tree;
	t->aI = *tmpAppInfo;
	t->aI2.app_name[0] = '~';
	t->left = NULL;
	t->mid = NULL;
	t->right = NULL;
	tree* parent = NULL;

	/*cout << t->aI.category << endl;
	cout << t->aI.app_name << endl;
	cout << t->aI.version << endl;
	cout << t->aI.units << endl;
	cout << t->aI.size << endl;
	cout << t->aI.price << endl;\
	cout << endl;*/
	//search for category
	
	for (int i = 0; i < tmpNumCategories; i++)
	{
		//cout << strcmp(tmpAppInfo->category, ptrArrCategories[i].category) << "STRNG COMPAAARRRIIIISSSOOOOONNNNNN" << endl;
		//cout << tmpAppInfo->category << endl;
		//cout << ptrArrCategories[i].category << endl;
		if (strcmp(tmpAppInfo->category, ptrArrCategories[i].category) == 0)//search for the category of the given app from the input in the arrCategories array
		{
			/*cout << "found category " << endl;
			cout << tmpAppInfo->category << endl*/;
			if(ptrArrCategories[i].root == NULL)//once found, if the root is null, set it
			{	
				//DEBUGGING
				//cout << "null root" << endl;
				//DEBUGGING^
				ptrArrCategories[i].root = t;
				return t;
			}
			else
			{
				tree* curr = ptrArrCategories[i].root;
				while(curr)
				{
					parent = curr;
					if (strcmp(t->aI.app_name, curr->aI.app_name) > 0)
					{
						curr = curr->right;
					}
					else
					{
						curr = curr->left;
					}
				}

				if (strcmp(t->aI.app_name, parent->aI.app_name) < 0)
				{
					parent->left = t;
					//cout << "parent left: " << parent->left->aI.app_name << endl;
					return t;
				}
				else
				{
					parent->right = t;
					//cout << "parent right: " << parent->right->aI.app_name << endl;
					return t;
				}
				//cout << ptrArrCategories[i].root << endl;
			}
			break;
		}
	}
}

int numCategories = 0;
int numApps = 0;
int lineCounter = 1;
int appInfoCounter = 1;

int parseInput1()
{
	for(string line; getline(cin, line);)//get numCategories from first line, incremented lineCounter and break
	{
		numCategories = atoi(line.c_str());
		//cout << numCategories << endl;
		lineCounter++;
		globalNumCategories = numCategories;
		return numCategories;
	}
}

categories* fillCategoriesArr(categories arrCategories[])
{
	
	for(string line; getline(cin, line);)
	{
		if (lineCounter > 1 && lineCounter < numCategories + 2)//read through list of categories stopping at line number containing the number of apps
		{
			//cout << line << endl;

			//populate array of category structs
			//array[lineCounter - 2] <---this gives accurate index of array the line should go in
			for (int i = 0; i < line.length(); i++)//copy the category name into the categories struct in the given index of arrCategories[]
			{
				arrCategories[lineCounter - 2].category[i] = line.c_str()[i];
			}
			lineCounter++;
			if (lineCounter == numCategories + 2)
			{
				return arrCategories;
			}
		}
	}
}

int parseInput2()
{	
	for(string line; getline(cin, line);)
	{
		if (lineCounter == numCategories + 2)//stop at line with number of apps and store that number
		{	//cout << numCategories << endl;
			//cout << lineCounter << endl;
			numApps = atoi(line.c_str());
			lineCounter++;
			if (lineCounter > numCategories + 2 && lineCounter < (numApps * 6 ) + (numCategories + 3))
			{

				//cout << "returned numapps " << numApps << endl;
				return numApps;
			}	
		}
	}
}

hash_table_entry* fillHT(categories arrCategories[], hash_table_entry hte[], int size)
{
	string strCategory, strApp_name, strVersion, strSize, strUnits, strPrice;
	
	for(string line; getline(cin, line);)
	{
		if (lineCounter > numCategories + 2 && lineCounter < (numApps * 6 ) + (numCategories + 3))//proceed through bulk of lines processing app_info struct data correctly
		{
			//cout<<line<<endl;
			if (appInfoCounter == 1)
			{
				strCategory = line;
				lineCounter++;
				
			}
			else if (appInfoCounter == 2)
			{
				strApp_name = line;
				lineCounter++;
				
			}
			else if (appInfoCounter == 3)
			{
				strVersion = line;
				lineCounter++;
				
			}
			else if (appInfoCounter == 4)
			{
				strSize = line;
				lineCounter++;
				
			}
			else if (appInfoCounter == 5)
			{
				strUnits = line;
				lineCounter++;
				
			}
			else if (appInfoCounter == 6)
			{
				//test print for category array (must manually enter size of array)
				/*for (int i = 0; i < 24; i++)
				{
					//cout << endl;
					//cout << "Summary of categories array: " << endl;
					//cout << "Category: " << arrCategories[i].category << endl;
					//cout << "Root: " << arrCategories[i].root << endl;
				}*/
				strPrice = line;
				appInfoCounter = 0;

				
				//makeApp_info(strCategory, strApp_name, strVersion, strSize, strUnits, strPrice);
				tree* tmp = insertTree(makeApp_info(strCategory, strApp_name, strVersion, strSize, strUnits, strPrice), arrCategories, numCategories);
				//convert strApp_name to char for ht entry
				char tmpApp_name[ APP_NAME_LEN ];
				for (int k = 0; k < APP_NAME_LEN; k++)
				{
					tmpApp_name[k] = ' ';
				}










				for (int i = 0; i < strApp_name.length(); i++)
				{
					tmpApp_name[i] = strApp_name.c_str()[i];
				}
				int hteIndex = hashFunction(tmpApp_name, size);
				while (hte[hteIndex].app_node != NULL)
				{
					if(hteIndex == size - 1)
					{
						hteIndex = 0;
					}
					else
					{
						hteIndex = hteIndex + 1;
					}
				}
				hte[hteIndex].app_node = tmp;
				for (int i = 0; i < APP_NAME_LEN - 1; i++)
				{
					hte[hteIndex].app_name[i] = tmpApp_name[i];
				}
				strCategory.clear(); strApp_name.clear(); strVersion.clear(); strSize.clear(); strUnits.clear(); strPrice.clear();
				lineCounter++;
				if (lineCounter == (numApps * 6 ) + (numCategories + 3))
				{
					return hte; 
				}
			}
			appInfoCounter++;
		}
	}
	
}





	/*	else if (lineCounter == (numApps * 6 ) + (numCategories + 3))//stop at number of instructions (near end of file) and store that number
		{
			//store number of instructions (this line contains the number of instructions
			lineCounter++;
		}
		else if (lineCounter > (numApps * 6 ) + (numCategories + 3))//process instructions (the rest of the file)
		{
			//process instructions (the rest of the file)
			lineCounter++;
		}

	}*/



	/*//test print for category array (must manually enter size of array)
	for (int i = 0; i < 24; i++)
	{
		cout << endl;
		cout << "Summary of categories array: " << endl;
		cout << "Category: " << arrCategories[i].category << endl;
		cout << "Root: " << arrCategories[i].root << endl;
	}*/
	/*int g = 1;
	cout << "Root: " << arrCategories[g].root->aI.app_name << endl;
	cout << "Left: " << arrCategories[g].root->left->aI.app_name << endl;
	cout << "Right: " << arrCategories[g].root->right->aI.app_name << endl;
	//cout << "Left of Left:  " << arrCategories[g].root->left->left->aI.app_name << endl;

	cout << "Left of Right: " << arrCategories[g].root->right->left->aI.app_name << endl;
	//cout << "Left of Left of Left: " << arrCategories[g].root->left->left->left->aI.app_name << endl;
	//cout << "Right of Left of Left: " << arrCategories[g].root->left->left->right->aI.app_name << endl;*/



void findApp_Name()
{

}
void findCategoryCategory_name()
{

}
void findPriceFree()
{

}
void rangeCategoryPriceLoHi()
{

}
void rangeCategoryApp_nameLoHi()
{

}
void deleteCategoryApp_name()
{
	//might not have to do this one
}

bool isLeaf(tree* t)
{
	if(t->left == NULL && t->mid == NULL && t->right == NULL)
	{
		return true;
	}
	else return false;
}

tree* addToLeaf(tree* t, tree* currSubRoot)
{
	//if the right key is null, add to current leaf
	if(t->aI2.app_name[0] == '\0')
	{
		
		
	}
		
}


//this will always return the pointer to the newly added node
tree* twoThreeIfy(app_info ai, tree* currSubRoot, categories tarrCategories[], int index)
{
		
	if(currSubRoot == NULL)
	{
		tree* hyphen = new tree;
		hyphen->aI = ai;
		//this is where the 2-3 array gets its root set
		tarrCategories[index].root = hyphen;
		return hyphen;
	}
	if(isLeaf(currSubRoot))
	{

		//return addToLeaf();
	}
		
}

	/*for(int i = 0; i < CAT_NAME_LEN; i++)
	{
		category[i] = t->aI.category[i];
	}
	for(int i = 0; i < APP_NAME_LEN; i++)
	{
		app_name[i] = t->aI.app_name[i];
	}
	cout << category << endl;
	cout << app_name << endl;
	cout << endl;*/





int main()
{
	categories *arrCategories = new categories[parseInput1()];
	arrCategories = fillCategoriesArr(arrCategories);

	int HTSize = getTableSize(parseInput2());
	hash_table_entry* theHashTable = new hash_table_entry[HTSize];


	//initialize structs in hashtable
	for (int i = 0; i < HTSize; i++)
	{
		theHashTable[i].app_node = NULL;
		for (int j = 0; j < APP_NAME_LEN; j++)
		{
			theHashTable[i].app_name[j] = ' ';
		}
	}
	theHashTable = fillHT(arrCategories, theHashTable, HTSize);
	///END BST OPERATIONS (STAGING)





	//BEGIN CREATION OF 2-3 TREES WITH APP INFO FROM THE BST HASH TABLE (NO POINTERS, JUST APP INFO)

	//new 2-3 categories array. copy the elements from the BST categories array, but set the root pointers to null
	categories *tarrCategories = new categories[globalNumCategories];
	for(int i = 0; i < globalNumCategories; i++)
	{
		tarrCategories[i] = arrCategories[i];
		tarrCategories[i].root = NULL;

	}



	//go through the hash table and find nonempty elements, match them with an element in the 2-3 categories array, and pass the root of the array element and the app info to the twoThreeIfy function
	for (int i = 0; i < HTSize; i++)
	{
		if(theHashTable[i].app_node != 0)
		{
			for(int j = 0; j < globalNumCategories; j++)
			{
				if(strcmp(tarrCategories[j].category, theHashTable[i].app_node->aI.category) == 0)
				{
					//the hash table (reusing the old BST one) will have its pointer reset following the insert
					theHashTable[i].app_node = twoThreeIfy(theHashTable[i].app_node->aI, tarrCategories[j].root, tarrCategories, j);
				}
			}
		}
	}


	//teeeeeeeeest
	for (int i = 0; i < globalNumCategories; i++)
	{
		cout << endl;
		cout << "Summary of categories array: " << endl;
		cout << "Category: " << tarrCategories[i].category << endl;
		cout << "Root: " << tarrCategories[i].root << endl;
	}


	/*//deletes all nodes
	for (int i = 0; i < HTSize; i++)
	{
		delete theHashTable[i].app_node;
		theHashTable[i].app_node = NULL;
	}*/



	return 0;



}